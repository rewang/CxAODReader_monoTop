#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"
#include <CxAODReader_monoTop/AnalysisReader_monoTop.h>
#include <CxAODReader_monoTop/sampleCS.h>
#include "xAODEventInfo/EventInfo.h"
#include "xAODBTagging/BTagging.h"

#include "TLorentzVector.h"
#include "TSystem.h"
#include "TRandom3.h"

#include "CxAODTools/EventSelection.h"

#include "CxAODTools_monoTop/BaseSelectionMonoTop.h"
#include "CxAODTools_monoTop/BaseSelectionMonoTop_FatJetSysts.h"
#include "zerolepton.h"
#include "onelepton.h"
#include "twolepton.h"

#define length(array) (sizeof(array)/sizeof(*(array)))

JetSubStructureUtils::BosonTag m_WTagger("medium", "smooth", "$ROOTCOREBIN/data/JetSubStructureUtils/config_13TeV_Wtagging_MC15_Prerecommendations_20150809.dat", false, false);
JetSubStructureUtils::BosonTag m_ZTagger("medium", "smooth", "$ROOTCOREBIN/data/JetSubStructureUtils/config_13TeV_Ztagging_MC15_Prerecommendations_20150809.dat", false, false);
TRandom3 m_random(0);

// this is needed to distribute the algorithm to the workers
ClassImp(AnalysisReader_monoTop)

AnalysisReader_monoTop :: AnalysisReader_monoTop () :
    AnalysisReader()
{
    // default value: blinded
    m_blinding = true;
    m_fullHists = false;
    m_doCutflow = false;
    m_doSyst = false;
    m_doShapeVariables = false;
    m_QCDHists = false;

    m_caloBTagLimit = 0.8244273;
    m_trkBTagLimit = 0.6455;
    m_RadToDeg = 180./TMath::Pi();
    m_DegToRad = TMath::Pi()/180.;

    m_variationName.insert(std::pair<int,std::string>(1,"sup"));
    m_variationName.insert(std::pair<int,std::string>(0,"r"));
    m_variationName.insert(std::pair<int,std::string>(-1,"sdo"));

    m_variableName.insert(std::pair<int,std::string>(0,"_je"));
    m_variableName.insert(std::pair<int,std::string>(1,"_jm"));
    m_variableName.insert(std::pair<int,std::string>(2,"_jd2"));
}

EL::StatusCode AnalysisReader_monoTop :: histInitialize ()
{

    Info("histInitialize()", "Initializing histograms with monoTop setup.");

    //NEW
    // histogram manager
    m_histSvc = new HistSvc();
    m_histNameSvc = new HistNameSvc_monoTop();
    m_histSvc -> SetNameSvc(m_histNameSvc);
    m_histSvc -> SetWeightSysts(&m_weightSysts);

    bool fillHists = true;
    m_histSvc->SetFillHists(fillHists);

    return EL::StatusCode::SUCCESS;

}

double AnalysisReader_monoTop :: checkWeight(const double& weight)
{

    if(weight<-1000) {
        std::cout << "negative weight: " << weight << std::endl;
        return 0;
    }
    return weight;
}

bool AnalysisReader_monoTop :: checkBlinding(const double& m)
{
    // return true if event is in the blinded region
    // blinding is hard-coded to prevent accidental unblinding
    // before our analysis is allowed to unblind
    bool inBlindedRegion = (m>70000 && m<140000);
    bool blindEvent = false;

    if(!m_isMC && m_analysisType == "0lep" && inBlindedRegion)
        blindEvent = true;

    if(m_blinding && inBlindedRegion)
        blindEvent = true;

    return blindEvent;
}

// trying out the OL removal fix
PROPERTY( PropsEmptyContFix , float , pt )
PROPERTY( PropsEmptyContFix , float , px )
EL::StatusCode AnalysisReader_monoTop :: clearEmptyContainersWithNonZeroSize ()
{
    // empty containers for replacement
    static const xAOD::ElectronContainer      empty_electrons ;
    static const xAOD::PhotonContainer        empty_photons   ;
    static const xAOD::MuonContainer          empty_muons     ;
    static const xAOD::TauJetContainer        empty_taus      ;
    static const xAOD::JetContainer           empty_jets      ;
    static const xAOD::TruthParticleContainer empty_truthParts;

    // replace invalid particle containers with empty ones
    if (m_electrons ) if (m_electrons ->size()) if (!PropsEmptyContFix::pt.exists(m_electrons ->at(0))) m_electrons  = &empty_electrons ;
    if (m_photons   ) if (m_photons   ->size()) if (!PropsEmptyContFix::pt.exists(m_photons   ->at(0))) m_photons    = &empty_photons   ;
    if (m_muons     ) if (m_muons     ->size()) if (!PropsEmptyContFix::pt.exists(m_muons     ->at(0))) m_muons      = &empty_muons     ;
    if (m_taus      ) if (m_taus      ->size()) if (!PropsEmptyContFix::pt.exists(m_taus      ->at(0))) m_taus       = &empty_taus      ;
    if (m_jets      ) if (m_jets      ->size()) if (!PropsEmptyContFix::pt.exists(m_jets      ->at(0))) m_jets       = &empty_jets      ;
    if (m_fatJets   ) if (m_fatJets   ->size()) if (!PropsEmptyContFix::pt.exists(m_fatJets   ->at(0))) m_fatJets    = &empty_jets      ;
    if (m_trackJets ) if (m_trackJets ->size()) if (!PropsEmptyContFix::pt.exists(m_trackJets ->at(0))) m_trackJets  = &empty_jets      ;
    if (m_truthParts) if (m_truthParts->size()) if (!PropsEmptyContFix::px.exists(m_truthParts->at(0))) m_truthParts = &empty_truthParts;

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_monoTop :: initializeSelection()
{
    Info("initializeSelection()", "Initialize selection.");

    m_blinding = false;
    try {
        m_config->getif<bool>("blinding", m_blinding);
    } catch (int e) {
        Error("initializeSelection()", "an exception occurred while reading the blinding config: %d", e);
    }
    Info("initializeSelection()", "Initialize blinding %d.", m_blinding);


    m_doSyst = false;
    try {
        m_config->getif<bool>("doSyst", m_doSyst);
    } catch (int e) {
        Error("initializeSelection()", "an exception occurred while reading the doSyst config: %d", e);
    }
    Info("initializeSelection()", "Initialize doSyst %d.", m_doSyst);


    m_fullHists = false;
    try {
        m_config->getif<bool>("fullHists", m_fullHists);
    } catch (int e) {
        Error("initializeSelection()", "an exception occurred while reading the fullHists config: %d", e);
    }
    Info("initializeSelection()", "Initialize fullHists %d.", m_fullHists);


    m_doCutflow = false;
    try {
        m_config->getif<bool>("doCutflow", m_doCutflow);
    } catch (int e) {
        Error("initializeSelection()", "an exception occurred while reading the doCutflow config: %d", e);
    }
    Info("initializeSelection()", "Initialize doCutflow %d.", m_doCutflow);


    m_doShapeVariables = false;
    try {
        m_config->getif<bool>("doShapeVariables", m_doShapeVariables);
    } catch (int e) {
        Error("initializeSelection()", "an exception occurred while reading the doShapeVariables: %d", e);
    }
    Info("initializeSelection()", "Initialize doShapeVariables %d.", m_doShapeVariables);


    m_QCDHists = false;
    try {
        m_config->getif<bool>("QCDHists", m_QCDHists);
    } catch (int e) {
        Error("initializeSelection()", "an exception occurred while reading the QCD histogram config: %d", e);
    }
    Info("initializeSelection()", "Initialize QCDHists %d.", m_QCDHists);

    m_config->getif<string>("analysisType", m_analysisType);
    Info("initializeSelection()", "Initialize analysisType '%s'.", m_analysisType.c_str());
    m_config->getif<string>("analysisRegion", m_analysisRegion);
    Info("initializeSelection()", "Initialize analysisRegion '%s'.", m_analysisRegion.c_str());


    // set event selection:
    if(m_analysisRegion=="monoTop_ZeroLepton") {
        m_eventSelection = new BaseSelectionMonoTop();
        m_fillFunction = std::bind(&AnalysisReader_monoTop::fill_monoTop_ZeroLepton, this);
    }
    else if(m_analysisRegion=="monoTop_OneLepton") {
        m_eventSelection = new BaseSelectionMonoTop();
        m_fillFunction = std::bind(&AnalysisReader_monoTop::fill_monoTop_OneLepton, this);
    }
    else if(m_analysisRegion=="monoTop_TwoLepton") {
        m_eventSelection = new BaseSelectionMonoTop();
        m_fillFunction = std::bind(&AnalysisReader_monoTop::fill_monoTop_TwoLepton, this);
    }
    else {

        Error("initializeSelection()", "Invalid analysisRegion %s", m_analysisRegion.c_str());
        return EL::StatusCode::FAILURE;
    }

    m_overlapRemoval = new OverlapRemoval(*m_config);
    EL_CHECK("AnalysisReader_monoTop::initializeSelection()", m_overlapRemoval -> initialize());

    m_config->getif<std::vector<std::string> >("csCorrections", m_csCorrections);
    m_config->getif<std::vector<std::string> >("csVariations", m_csVariations);

    // we just want to apply variations, and NO corrections
    m_csCorrections.clear();

    if(m_csCorrections.size()==0) Info("AnalysisReader_monoTop ::"," Running CorrsAndSysts --- no corrections found! ---");
    for(int i=0; i<(int)m_csCorrections.size(); i++) {
        Info("AnalysisReader_monoTop ::", "Running CorrsAndSysts corrections: '%s'",m_csCorrections.at(i).c_str());
    }
    if(m_csVariations.size()==0) Info("AnalysisReader_monoTop ::"," Running CorrsAndSysts --- no variations found! ---");
    for(int i=0; i<(int)m_csVariations.size(); i++) {
        Info("AnalysisReader_monoTop ::"," Running CorrsAndSysts variations: '%s'",m_csVariations.at(i).c_str());
    }

    return EL::StatusCode::SUCCESS;
}


EL::StatusCode AnalysisReader_monoTop::initializeCorrsAndSysts ()
{
    if (!m_isMC) return EL::StatusCode::SUCCESS;
    Info("initializeCorrsAndSysts()", "Initialize CorrsAndSysts.");

    std::string comEnergy    = m_config->get<std::string>("COMEnergy");

    TString csname;
    std::string debugname;

    if (m_analysisType == "0lep") {
        csname = comEnergy + "_ZeroLepton";
        debugname = comEnergy + "_ZeroLepton";
    }
    if (m_analysisType == "1lep") {
        csname = comEnergy + "_OneLepton";
        debugname =  comEnergy + "_OneLepton";
    }
    if (m_analysisType == "2lep") {
        csname = comEnergy + "_TwoLepton";
        debugname =  comEnergy + "_TwoLepton";
    }

    Info("initializeCorrsAndSysts()", "Initializing CorrsAndSysts for %s", debugname.c_str());

    m_corrsAndSysts = new CorrsAndSysts(csname);

    return EL::StatusCode::SUCCESS;
} // initializeCorrsAndSysts


EL::StatusCode AnalysisReader_monoTop::initializeIsMC()
{
    Info("initializeIsMC()", "Initialize isMC.");

    // get nominal event info
    // -------------------------------------------------------------
    const xAOD::EventInfo *eventInfo = m_eventInfoReader->getObjects("Nominal");

    if (!eventInfo) return EL::StatusCode::FAILURE;

    // get MC flag - different info on data/MC files
    // -----------------------------------------------
    m_isMC = Props::isMC.get(eventInfo);
    Info("initializeIsMC()", "isMC = %i", m_isMC);


    // try to obtain cross section from FrameworkSub/data
    std::string comEnergy    = m_config->get<std::string>("COMEnergy");
    if ((comEnergy != "8TeV") || (comEnergy != "7TeV") || (comEnergy != "13TeV")) comEnergy = "13TeV";
    std::string xSectionFile = gSystem->Getenv("ROOTCOREBIN");
    xSectionFile      += "/data/FrameworkSub/XSections_";
    xSectionFile      += comEnergy;
    xSectionFile      += ".txt";

    // if cross section is set manually, take manually defined path instead
    m_config->getif<std::string>("xSectionFile", xSectionFile);

    m_xSectionProvider = new XSectionProvider(xSectionFile);

    if (!m_xSectionProvider) {
        Error("initializeXSections()", "XSection provider not initialized!");
        return EL::StatusCode::FAILURE;
    }

    return EL::StatusCode::SUCCESS;
} // initializeIsMC


EL::StatusCode AnalysisReader_monoTop::initializeSumOfWeights ()
{
    Info("initializeSumOfWeights()", "Initialize sum of weights.");

    if (!m_isMC) {
        return EL::StatusCode::SUCCESS;
    }

    std::string sumOfWeightsFile;
    bool generateYieldFile = false;
    m_config->getif<bool>("generateYieldFile", generateYieldFile);

    std::string mcPeriod;
    m_config->getif<std::string>("mcPeriod", mcPeriod);
    if (generateYieldFile && (m_sumOfWeightsFile != "")) {
        sumOfWeightsFile = m_sumOfWeightsFile;
    } else {
        std::string comEnergy = m_config->get<std::string>("COMEnergy");
        std::string analysisType = m_analysisType; //
        std::string yieldFile;
        yieldFile  = "/data/FrameworkSub/yields.";
        yieldFile += analysisType;
        yieldFile += ".";
        yieldFile += comEnergy;
        if     (mcPeriod == "mc15b") yieldFile += "_mc15b";
        else if (mcPeriod == "mc15c") yieldFile += "_mc15c";
        yieldFile += ".txt";
        m_config->getif<string>("yieldFile", yieldFile);
        sumOfWeightsFile  = gSystem->Getenv("ROOTCOREBIN");
        sumOfWeightsFile += yieldFile;
    }

    m_sumOfWeightsProvider = new sumOfWeightsProvider(sumOfWeightsFile);

    if (!m_sumOfWeightsProvider) {
        Error("initializeSumOfWeights()", "SumOfWeights not initialized!");
        return EL::StatusCode::FAILURE;
    }
    return EL::StatusCode::SUCCESS;
} // initializeSumOfWeights


EL::StatusCode AnalysisReader_monoTop::initializeTools ()
{
    EL_CHECK("AnalysisReader_monoTop::initializeTools()", AnalysisReader::initializeTools());

    // -------------
    // trigger tool
    // -------------
    m_triggerTool = new TriggerTool(*m_config, m_config->get<std::string>("analysisType"));
    EL_CHECK("AnalysisReader::initializeTools()", m_triggerTool->initialize());
    m_triggerSystList = m_triggerTool->getTriggerSystList();

    return EL::StatusCode::SUCCESS;
} // initializeTools

EL::StatusCode AnalysisReader_monoTop::applyCS (float VpT,
        float Mbb,
        float truthPt,
        float DeltaPhi,
        float DeltaR,
        float pTB1,
        float pTB2,
        float MET,
        float avgTopPt,
        int   njet,
        int   ntag)
{

    std::string detailSampleNameClean = m_xSectionProvider->getSampleDetailName(m_mcChannel);
    std::string sampleNameClean       =  m_histNameSvc->get_sample();
    sampleCS sample("temp");
    sample.setup_sample(sampleNameClean, detailSampleNameClean);
    std::string flav = "";

    if ((sampleNameClean == "Z") || (sampleNameClean == "W")) {
        int flav0, flav1 = -1;
        m_histNameSvc->get_eventFlavour(flav0,flav1);

        if(flav0 < 0 || flav1 < 0) {
            Error("applyCS()","Failed to retrieve event flavour! Exiting!");
            return EL::StatusCode::FAILURE;
        } else if (flav0 == 5 || flav1 == 5) flav = "b";
        else if (flav0 == 4 || flav1 == 4) {
            flav = "c";
            if(flav0 == flav1)  flav = "cc";
        } else if (flav0 < 4 && flav1 < 4) flav = "l";
    }

    //std::cout<<"sampleName: "<<sample.get_sampleName()<<"  "<<flav<<std::endl;

    TString sampleName                 = sample.get_sampleName() + flav;
    TString detailsampleName           = sample.get_detailsampleName();

    //std::cout<<"csSampleName: "<<sampleName<<"  "<<detailsampleName<<std::endl;

    CAS::EventType evtType             = m_corrsAndSysts->GetEventType(sampleName);
    CAS::DetailEventType detailevtType = m_corrsAndSysts->GetDetailEventType(detailsampleName);

    double weightUp                    = 1;
    double weightDo                    = 1;


    if (m_csVariations.size() != 0) { // apply variations
        for (decltype(m_csVariations.size()) i = 0; i < m_csVariations.size(); i++) {
            TString s_csVariations(m_csVariations[i]);

            //std::cout<<"Variation: "<<s_csVariations<<std::endl;

            CAS::Systematic sysUp;
            CAS::Systematic sysDo;
            CAS::SysVar     varUp = CAS::Up;
            CAS::SysVar     varDo = CAS::Do;
            CAS::SysBin     bin   = CAS::Any;

            m_corrsAndSysts->GetSystFromName(s_csVariations, sysUp, bin, varUp);
            m_corrsAndSysts->GetSystFromName(s_csVariations, sysDo, bin, varDo);

            //std::cout<<"csInputs: "<<VpT<<"  "<<Mbb<<"  "<<truthPt<<"  "<<DeltaPhi<<"  "<<DeltaR<<"  "<<pTB1<<"  "<<pTB2<<"  "<<MET<<"  "<<njet<<"  "<<ntag<<"  "<<detailevtType<<std::endl;

            weightUp = m_corrsAndSysts->Get_SystematicWeight(evtType, VpT, Mbb, truthPt, DeltaPhi, DeltaR, pTB1, pTB2, MET, njet, ntag, detailevtType, sysUp, varUp, bin);
            weightDo = m_corrsAndSysts->Get_SystematicWeight(evtType, VpT, Mbb, truthPt, DeltaPhi, DeltaR, pTB1, pTB2, MET, njet, ntag, detailevtType, sysDo, varDo, bin);

            //std::cout<<"CheckWeights: "<<weightUp<<"  "<<weightDo<<std::endl;

            if (s_csVariations.BeginsWith("Sys")) s_csVariations.Remove(0, 3);  // remove "Sys" from name to avoid duplicating it
            // Fill the histograms only if at least one side has a non-0 variation
            if(fabs(weightUp - 1) > 1.e-6 || fabs(weightDo - 1) > 1.e-6) {
                //std::cout << "weight up: " << weightUp << " weight down: "<< weightDo << std::endl;
                m_weightSysts.push_back({ (std::string)s_csVariations + "__1up",   static_cast<float>(weightUp) });
                m_weightSysts.push_back({ (std::string)s_csVariations + "__1down", static_cast<float>(weightDo) });
            }
        }
    }
    return EL::StatusCode::SUCCESS;
} // applyCS


EL::StatusCode AnalysisReader_monoTop::setObjectsForOR(const xAOD::ElectronContainer* electrons,
        const xAOD::PhotonContainer* photons,
        const xAOD::MuonContainer* muons,
        const xAOD::TauJetContainer* taus,
        const xAOD::JetContainer* jets,
        const xAOD::JetContainer* fatJets)
{

    //std::cout<<"SetORObjects :: monoTop"<<std::endl;

    //if (m_eventCounter < 2) {
    //  Warning("AnalysisReader_monoTop::setObjectsForOR", "Setting passPreSel to true for all particles!");
    //}
    //

    // Should copy analysis specific loose selection flags to passPreSel for overlap removal.
    // Here: just set all to true.
    if (electrons) {
        for (const xAOD::Electron* elec : *electrons) {
            Props::passPreSel.set(elec, Props::isVHLooseElectron.get(elec));
        }
    }

    if (muons) {
        for (const xAOD::Muon* muon : *muons) {
            Props::passPreSel.set(muon, Props::isVHLooseMuon.get(muon));
        }
    }

    if (jets) {
        for (const xAOD::Jet* jet : *jets) {
            Props::passPreSel.set(jet, Props::isVetoJet.get(jet));
        }
    }

    if (fatJets) {
        for (const xAOD::Jet* jet : *fatJets) {
            Props::passPreSel.set(jet, Props::isFatJet.get(jet));
        }
    }

    if (taus) {
        for (const xAOD::TauJet* tau : *taus) {
            Props::passPreSel.set(tau, Props::passTauSelector.get(tau));
        }
    }

    if (photons) {
        for (const xAOD::Photon* photon : *photons) {
            Props::passPreSel.set(photon, Props::isVBFLoosePhoton.get(photon));
        }
    }

    return EL::StatusCode::SUCCESS;
}




double AnalysisReader_monoTop :: GetPThrust(std::vector<const xAOD::Jet*> jets, const xAOD::Jet* fatjet)
{

    // separate calojets from fatjet
    std::vector<const xAOD::Jet*> calojets_separated;
    for (const xAOD::Jet* jet: jets) {
        if(fatjet->p4().DeltaR(jet->p4())>0.9) {
            calojets_separated.push_back(jet);
        }
    }

    // calculate PThrust
    double pthrust = 0;
    for (const xAOD::Jet* jet : calojets_separated) {
        double dPhi =  fabs(jet->p4().DeltaPhi(fatjet->p4()));
        pthrust += TMath::Cos(dPhi);
    }
    return pthrust;
}

void AnalysisReader_monoTop::truthVariablesCS(float &truthPt,float &avgTopPt)
{

    truthPt=0;
    avgTopPt=0;
    std::string samplename       =  m_histNameSvc->get_sample();

    if(samplename == "ttbar") { // average pT of top,antitop truth pair
        for (auto *part : *m_truthParts) {
            if(fabs(part->pdgId())==6) {
                avgTopPt += (part->pt()/2.0);
                truthPt += (part->pt()/2.0);
            }
        }
    }

    if(samplename =="Z" || samplename == "W") { // take truth pT of truth V candidate built from truth leptons
        TLorentzVector l1,l2,V;
        for (auto *part : *m_truthParts) {
            if(part->pdgId()==11 || part->pdgId()==12 || part->pdgId()==13 || part->pdgId()==14 || part->pdgId()==15 || part->pdgId()==16) l1 = part->p4();
            if(part->pdgId()==-11 || part->pdgId()==-12 || part->pdgId()==-13 || part->pdgId()==-14 || part->pdgId()==-15 || part->pdgId()==-16) l2 = part->p4();
        }
        V = l1 + l2;
        truthPt = V.Pt();
    }

    if(samplename == "WW" || samplename == "ZZ") { // cannot select the leptonic one -> take average pT
        for (auto *part : *m_truthParts) {
            if(fabs(part->pdgId())==23 || fabs(part->pdgId())==24) truthPt += (part->pt()/2.0);
        }
    }

    if(samplename == "WZ") { // can discriminate between leptonic and hadronic V -> take leptonic V truth pT
        int pdgId = -1;
        if(m_mcChannel==361083) pdgId=24; // WlvZqq
        if(m_mcChannel==361084) pdgId=23; // WqqZll
        if(m_mcChannel==361085) pdgId=23; // WqqZvv
        for (auto *part : *m_truthParts) {
            if(fabs(part->pdgId())==pdgId) truthPt = (part->pt());
        }
    }

} // truthVariablesCS

void AnalysisReader_monoTop :: SetFatJetFlavor(std::vector<const xAOD::Jet*> fatjets)
{
    // LASER: set the truth flavor composition tag for the leading fat jet in MC samples
    // Take the leading fat jet, find the ghost associated track jets, and look at the truth label for the two leading track jets
    // If there is only 1 track jet, take the truth label and put it in one of the single track jet categories
    // If there is no fat jet left after preselection, put it in the n category (for none)
    // There are a total of 10 categories: bb, bc, bl, cc, cl, ll, b, c, l, n

    int jet0flav = -1;
    int jet1flav = -1;

    // If there are no fat jets
    if (fatjets.size() == 0) {
        m_histNameSvc->set_eventFlavour(jet0flav, jet1flav); // No fat jets left, this will be labelled n
        return;
    }

    // grab the track jets and apply basic selection
    const xAOD::Jet *fatjet = fatjets.at(0);
    std::vector<const xAOD::Jet*> trkJets, goodTrkJets;
    if (fatjet->getAssociatedObjects<xAOD::Jet>("GhostAntiKt2TrackJet", trkJets)) {
        for (const xAOD::Jet *trkJ : trkJets) {
            if (!trkJ) continue; // if the trackjet is not valid then skip it
            if (!(trkJ->pt() / 1000. > 10. && fabs(trkJ->eta()) < 2.5)) continue;
            goodTrkJets.push_back(trkJ);
        }
    } else {
        m_histNameSvc->set_eventFlavour(jet0flav, jet1flav); // No associated track jets, this will be labelled n
        return;
    }

    if (goodTrkJets.size() == 0) {
        m_histNameSvc->set_eventFlavour(jet0flav, jet1flav); // No good track jets, this will be labelled n
        return;
    }

    // get HadronConeExclTruthLabelID from leading track jet(s)
    std::sort(goodTrkJets.begin(), goodTrkJets.end(), EventSelection::sort_pt);
    const xAOD::Jet *trkJ_lead = 0, *trkJ_sublead = 0;
    trkJ_lead = goodTrkJets.at(0);
    if (goodTrkJets.size() > 1) trkJ_sublead = goodTrkJets.at(1);
    jet0flav = Props::HadronConeExclTruthLabelID.get(trkJ_lead);
    if (trkJ_sublead) jet1flav = Props::HadronConeExclTruthLabelID.get(trkJ_sublead);
    m_histNameSvc->set_eventFlavour(jet0flav, jet1flav);
    return;
}

void AnalysisReader_monoTop::SetBTagTool(std::vector<const xAOD::Jet*> jets, std::string configList)
{

    std::vector<std::string> bTagToolConfig;
    m_config->getif<std::vector<std::string> >(configList, bTagToolConfig);
    m_bTagTool->setTagger(bTagToolConfig[0]);
    m_bTagTool->setTaggerEfficiency(std::stod(bTagToolConfig[1]));
    m_bTagTool->setJetAuthor(bTagToolConfig[2]);
    m_bTagTool->setTaggingScheme(bTagToolConfig[3]);

    for(auto jet : jets) {
        BTagProps::isTagged.set(jet, static_cast<decltype(BTagProps::isTagged.get(jet))>(m_bTagTool->isTagged(*jet)));
        BTagProps::tagWeight.set(jet, Props::MV2c10.get(jet));
    }
}

void AnalysisReader_monoTop :: compute_btagging_calo (std::vector<const xAOD::Jet*> jets)
{
    if(jets.size()>0) {
        SetBTagTool(jets, "bTagToolConfigs_calo");
    }
}

void AnalysisReader_monoTop :: compute_btagging_track (std::vector<const xAOD::Jet*> jets)
{
    if(jets.size()>0) {
        SetBTagTool(jets, "bTagToolConfigs_track");
    }
}

bool AnalysisReader_monoTop :: isBoostedJetTagged(double jpt, double jmass, double jtau32)
{
    //etaRegion==2, targetEff=80%
    float jetPtCut[] = { 250000.000,325000.000,375000.000,425000.000,475000.000,525000.000,575000.000,625000.000,675000.000,725000.000,775000.000,850000.000,950000.000,1100000.000,1300000.000,1680000.000 };
    float TopTagSmoothMassCut[] = {67888.967,72014.026,74764.066,76769.667,78354.344,79170.000,79530.000,80158.525,81195.851,82779.245,84890.965,88747.162,94262.629,102710.787,113868.253,135067.438  };
    float TopTagSmoothTau32Cut[] = { 0.879,0.831,0.799,0.770,0.746,0.727,0.714,0.706,0.701,0.698,0.698,0.699,0.700,0.701,0.699,0.696  };

    std::vector<float> m_jetPtCut(std::begin(jetPtCut), std::end(jetPtCut));
    std::vector<float> m_TopTagSmoothMassCut(std::begin(TopTagSmoothMassCut), std::end(TopTagSmoothMassCut));
    std::vector<float> m_TopTagSmoothTau32Cut(std::begin(TopTagSmoothTau32Cut), std::end(TopTagSmoothTau32Cut));

    float mass_cut = InterpolatorVal(jpt, m_jetPtCut, m_TopTagSmoothMassCut);
    float tau32_cut = InterpolatorVal(jpt, m_jetPtCut, m_TopTagSmoothTau32Cut);

    if( (jtau32<tau32_cut) && (jmass>mass_cut) ) return true;
    else return false;
}



float AnalysisReader_monoTop :: InterpolatorVal(float x, std::vector<float> m_xvalues, std::vector<float> m_yvalues)
{
    float m_xmin(0),m_xmax(0),m_ymin(0),m_ymax(0);

    if(! m_xvalues.empty()) {
        m_xmin = m_xvalues.front();
        m_xmax = m_xvalues.back();
    }
    if(! m_yvalues.empty()) {
        m_ymin = m_yvalues.front();
        m_ymax = m_yvalues.back();
    }

    if(x<m_xmin) return m_ymin;
    if(x>m_xmax) return m_ymax;

    int i=0;
    float xlow, xup;
    while(m_xvalues[i+1]<x) i++;
    xlow = m_xvalues[i];
    xup = m_xvalues[i+1];
    float a = (m_yvalues[i+1] - m_yvalues[i])/( xup - xlow );
    float b = m_yvalues[i] - a*xlow;
    return a*x+b;
}



double AnalysisReader_monoTop :: GetMETTriggerSF( const double& MET )
{

    double SF = 1;

    if(MET>0   && MET<=70 )       SF = 0.415415;
    else if(MET>70  && MET<=80 )  SF = 0.506634;
    else if(MET>80  && MET<=90 )  SF = 0.598941;
    else if(MET>90  && MET<=100)  SF = 0.726115;
    else if(MET>100 && MET<=110)  SF = 0.801848;
    else if(MET>110 && MET<=120)  SF = 0.898925;
    else if(MET>120 && MET<=130)  SF = 0.966636;
    else if(MET>130 && MET<=140)  SF = 0.989466;
    else if(MET>140 && MET<=150)  SF = 1.01815;
    else if(MET>150 && MET<=160)  SF = 1.02394;
    else if(MET>160 && MET<=170)  SF = 1.01947;
    else if(MET>170 && MET<=180)  SF = 1.01384;
    else if(MET>180 && MET<=190)  SF = 1.01279;
    else if(MET>190 && MET<=200)  SF = 1.00877;
    else if(MET>200 && MET<=210)  SF = 1.00485;
    else if(MET>210 && MET<=220)  SF = 1.00226;
    else if(MET>220 && MET<=230)  SF = 1.00038;
    else if(MET>230 && MET<=240)  SF = 1.00028;
    else if(MET>240 && MET<=250)  SF = 1.00034;
    else if(MET>250 && MET<=260)  SF = 1.00031;
    else if(MET>260 && MET<=270)  SF = 1.00019;
    else if(MET>270 && MET<=280)  SF = 0.999455;
    else if(MET>280 && MET<=290)  SF = 1.00015;
    else if(MET>290 && MET<=300)  SF = 1.00008;
    else if(MET>300 && MET<=320)  SF = 0.998781;
    else if(MET>320 && MET<=360)  SF = 1.00045;
    else if(MET>360 && MET<=400)  SF = 0.999911;
    else if(MET>400 && MET<=450)  SF = 0.997301;
    else if(MET>450 && MET<=500)  SF = 1;
    else if(MET>500 && MET<=600)  SF = 1;
    else if(MET>600 && MET<=600)  SF = 1;
    else if(MET>700 && MET<=600)  SF = 1;
    else if(MET>800 && MET<=600)  SF = 1;
    else if(MET>900 && MET<=600)  SF = 1;
    else if(MET>1000)             SF = 1;

    return SF;

}
