
EL::StatusCode AnalysisReader_monoTop :: fill_monoTop_ZeroLepton()
{

    float METBins[]= {200,230,260,290,320,350,380,410,440,470,500,530,560,590,620,650,680,710,740,770,800,850,900,950,1000,1050,1100,1200,1300,1500,1700,2000};
    const int nBinsMET = sizeof(METBins)/sizeof(float) - 1;


    float METBins2[]= {250,280,310,340,370,400,430,460,490,520,550,600,650,700,800,900,1000,1500,2000};
    const int nBinsMET2 = sizeof(METBins2)/sizeof(float) - 1;


    // get preselection results
    ResultBase selectionResult = ((BaseSelectionMonoTop*)m_eventSelection)->result();

    std::vector<const xAOD::Electron*> electrons = selectionResult.electrons;
    std::vector<const xAOD::Muon*> muons         = selectionResult.muons;
    std::vector<const xAOD::Photon*> photons     = selectionResult.photons;
    std::vector<const xAOD::Jet*> jets           = selectionResult.jets;
    std::vector<const xAOD::Jet*> fatjets        = selectionResult.fatjets;
    std::vector<const xAOD::Jet*> trackjets      = selectionResult.trackjets;
    const xAOD::MissingET* met                   = selectionResult.met;

    TLorentzVector metVec;
    metVec.SetPxPyPzE(met->mpx(), met->mpy(), 0, met->met());

    //this is track-based MissingET
    TLorentzVector mptVec;
    mptVec.SetPxPyPzE(m_mpt->mpx(), m_mpt->mpy(), 0, m_mpt->met());


    ///////////////////////////////////////////////////
    //set up descriptors for the HistSvc
    m_histNameSvc->set_description("Boosted");
    // note: disregarding convention we set nfat =1 for all events
    m_histNameSvc->set_nFatJet(1);
    m_histNameSvc->set_pTV(-1);
    ///////////////////////////////////////////////////



    // truth information
    TString samplename_ = m_histNameSvc->get_sample();
    if(samplename_.Contains("ResWjj") || samplename_.Contains("NonresWjj")) {

        TLorentzVector genMET(0,0,0,0);
        for (auto *part : *m_truthParts) {
            int pdgid = fabs(part->pdgId());
            int status = part->status();
            if(status==1 && pdgid!=1000022) genMET += part->p4();

        }
        m_histSvc->BookFillVarHist("genmetrebin", nBinsMET2, &METBins2[0], genMET.Pt()/1000.0, m_weight, true);
    }




    ////////////////////////////////
    //Sort jets into categories for usage in later calculations
    ////////////////////////////////
    std::vector<const xAOD::Jet*> jetsForward;
    std::vector<const xAOD::Jet*> jetsCentralBTagged;
    std::vector<const xAOD::Jet*> jetsCentralNotBTagged;
    for (const xAOD::Jet* jet: jets) {
        if(fabs(jet->p4().Eta())<2.5) {
            if(Props::MV2c10.get(jet)>m_caloBTagLimit)
                jetsCentralBTagged.push_back(jet);
            else
                jetsCentralNotBTagged.push_back(jet);
        } else
            jetsForward.push_back(jet);
    }

    std::vector<const xAOD::Jet*> jetsCentral = jetsCentralBTagged;
    jetsCentral.insert(jetsCentral.end(), jetsCentralNotBTagged.begin(), jetsCentralNotBTagged.end());

    std::vector<const xAOD::Jet*> alljets = jetsCentralBTagged;
    alljets.insert(alljets.end(), jetsCentralNotBTagged.begin(), jetsCentralNotBTagged.end());
    alljets.insert(alljets.end(), jetsForward.begin(), jetsForward.end());


    ////////////////////////////////
    //Set event flavour
    ////////////////////////////////
    int jet0flav=-1;
    int jet1flav=-1;
    std::vector<const xAOD::Jet*> jetsForEventFlavour = jetsCentralBTagged;
    jetsForEventFlavour.insert(jetsForEventFlavour.end(), jetsCentralNotBTagged.begin(), jetsCentralNotBTagged.end());
    jetsForEventFlavour.insert(jetsForEventFlavour.end(), jetsForward.begin(), jetsForward.end());

    if(jetsForEventFlavour.size()>=1 && m_isMC) {
        jet0flav = Props::HadronConeExclTruthLabelID.get(jetsForEventFlavour.at(0));
        if(jetsForEventFlavour.size()>=2) {
            jet1flav = Props::HadronConeExclTruthLabelID.get(jetsForEventFlavour.at(1));
        }
    }
    m_histNameSvc->set_eventFlavour(jet0flav, jet1flav);


    /////////////////////////////////
    //Get the Trigger
    /////////////////////////////////

    double triggerSF_nominal = 1.;
    bool triggerDecision = m_triggerTool->getTriggerDecision(m_eventInfo, triggerSF_nominal, 0, 0, 0, 0, m_met, 0, m_randomRunNumber, "Nominal");
    if(m_isMC) {
        m_weight *= triggerSF_nominal;
        // handle systematics
        if(m_currentVar == "Nominal") {
            double triggerSF;
            for (unsigned int i = 0; i < m_triggerSystList.size(); ++i) {
                triggerSF = 1.;
                if(!m_triggerTool->getTriggerDecision(m_eventInfo, triggerSF, 0, 0, 0, 0, m_met, 0, m_randomRunNumber, m_triggerSystList.at(i))) return EL::StatusCode::SUCCESS;
                if(triggerSF_nominal > 0) m_weightSysts.push_back({ m_triggerSystList.at(i), (float)(triggerSF / triggerSF_nominal) });
                else Error("fill_monoTop_ZeroLepton()", "Nominal trigger SF == 0!, The systematics will not be generated!");
            }
        }
    }
    if(triggerDecision==false) return EL::StatusCode::SUCCESS;




    ////////////////////////////
    // getting loose leptons for the different channels
    ////////////////////////////
    std::vector<const xAOD::Electron*> looseElectrons;
    looseElectrons.clear();
    for(const xAOD::Electron* elec: electrons) {
        if(Props::isVHLooseElectron.get(elec))  looseElectrons.push_back(elec);
    }

    std::vector<const xAOD::Muon*> looseMuons;
    looseMuons.clear();
    for(const xAOD::Muon* muon: muons) {
        if(Props::isVHLooseMuon.get(muon))   looseMuons.push_back(muon);
    }


    /////////////////////////////////
    //Get smallR jets not associated to leading largeR jet (dR>0.9)
    /////////////////////////////////
    std::vector<const xAOD::Jet*> calojets;
    std::vector<const xAOD::Jet*> calojets_btagged;
    std::vector<const xAOD::Jet*> calojets_separated;
    for (const xAOD::Jet* jet: alljets) {
        bool overlapRemove=false;
        for( const xAOD::Electron* elec: looseElectrons) {
            if(elec->p4().DeltaR(jet->p4()) < 0.4)
                overlapRemove = true;
        }
        if(overlapRemove) continue;

        calojets.push_back(jet);
        if( Props::MV2c10.get(jet)>m_caloBTagLimit) {
            calojets_btagged.push_back(jet);
        }

        if(fatjets.size()>=1) {
            if(fatjets.at(0)->p4().DeltaR(jet->p4())>0.9) {
                calojets_separated.push_back(jet);
            }
        }
    }


    //////////////////////////////////
    //top taggging and track tagging of Fatjet
    //////////////////////////////////
    bool isleadfatjettopbagged(false);

    std::vector<const xAOD::Jet*> fatjetsTopandBTagged;
    fatjetsTopandBTagged.clear();

    if(fatjets.size()>0) {
        m_histSvc->BookFillHist("massfatjet_raw",   100, 0.,500., fatjets.at(0)->m()/1000., m_weight);
        m_histSvc->BookFillHist("tau32_raw",   100, 0.,1., Props::Tau32.get(fatjets.at(0)), m_weight);
    }

    for (const xAOD::Jet* fatjet: fatjets) {

        float tau32 = Props::Tau32.get(fatjet);
        float jmass = fatjet->m();
        float jpt   = fatjet->pt();
        bool is_topbagged = isBoostedJetTagged(jpt, jmass, tau32);
        if(!is_topbagged) continue;

        int n_matched_trackjetsBTagged(0);
        //check the trackjet matching with the GhostAntiKt2TrackJet in Fatjet
        std::vector<const xAOD::Jet*> trkJets;
        if ((fatjet)->getAssociatedObjects<xAOD::Jet>("GhostAntiKt2TrackJet",trkJets) && trkJets.size() > 0) {
            for(const xAOD::Jet* trkJ : trkJets) {
                if(!trkJ) continue;

                bool found_matched_trkjet(false);
                bool is_trackjetsBTagged(false);
                for (const xAOD::Jet* jet: trackjets) {
                    double temp_dR = trkJ->p4().DeltaR(jet->p4());
                    m_histSvc->BookFillHist("trackjet_fatjetghost_dr",  1000,0,10, temp_dR, m_weight);
                    if(temp_dR==0) {
                        found_matched_trkjet = true;
                        is_trackjetsBTagged = (Props::MV2c10.get(jet)>m_trkBTagLimit);
                        break;
                    }
                }

                if(found_matched_trkjet && is_trackjetsBTagged) n_matched_trackjetsBTagged++;
            }
        }

        m_histSvc->BookFillHist("n_matched_trackjetsBTagged",  5,0,5, n_matched_trackjetsBTagged, m_weight);

        is_topbagged &= (n_matched_trackjetsBTagged==1);

        if(fatjet==fatjets.at(0)) isleadfatjettopbagged = is_topbagged;

        fatjetsTopandBTagged.push_back(fatjet);
    }

    std::sort(fatjetsTopandBTagged.begin(), fatjetsTopandBTagged.end(), EventSelection::sort_pt);

    m_histSvc->BookFillHist("nfatjetsTopandBTagged_raw",  5,0,5, fatjetsTopandBTagged.size(), m_weight);





    std::vector<const xAOD::Jet*> bjetsOutsideLeadfatjet;
    bjetsOutsideLeadfatjet.clear();

    for (const xAOD::Jet* jet: jetsCentralBTagged) {
        if(fatjetsTopandBTagged.size()==1) {
            double temp_dR = jet->p4().DeltaR(fatjetsTopandBTagged.at(0)->p4());
            if(temp_dR>0.9) bjetsOutsideLeadfatjet.push_back(jet);
        }
    }



    //////////////////////////////////
    //Test of btagging sf weights
    //////////////////////////////////
    //feed in the set of possible track jets that we may compute weights from
    //decorate jets with a weight
    //calculate the multiplication of the weight of all track jets that you give it
    compute_btagging_calo(jetsCentral);
    double b_tagging_sf_calo = computeBTagSFWeight(jetsCentral, m_jetReader->getContainerName());
    compute_btagging_track(trackjets);
    double b_tagging_sf_track = computeBTagSFWeight(trackjets, m_trackJetReader->getContainerName());
    if(!m_doSyst) {
        m_histSvc->BookFillHist("b_tagging_sf_calo",  100,0,4, b_tagging_sf_calo, m_weight);
        m_histSvc->BookFillHist("b_tagging_sf_track",  100,0,4, b_tagging_sf_track, m_weight);
    }


    /////////////////////////////////////////
    // Calculate angular distances for cuts
    /////////////////////////////////////////
    // minimum azimuthal distance between leading 3 jets and MET
    // to veto QCD process
    double min_dPhi = TMath::Pi();
    std::vector<const xAOD::Jet*> jetsForMindPhi;
    jetsForMindPhi.clear();
    unsigned int jetCount = 3;
    if (alljets.size()<jetCount) jetCount = alljets.size();
    for (unsigned int i=0; i<jetCount ; i++) {
        jetsForMindPhi.push_back(alljets.at(i));
    }
    for (const xAOD::Jet* jet: jetsForMindPhi) {
        float dphitemp = fabs(jet->p4().DeltaPhi(metVec));
        if( dphitemp < min_dPhi) {
            min_dPhi = dphitemp;
        }
    }


    //////////////////////////////
    //apply the CorrsAndSysts corrections
    //////////////////////////////
    //std::cout<<"Going apply CorrsAndSysts"<<std::endl;
    if (m_isMC && ((m_csCorrections.size() != 0) || (m_csVariations.size() != 0))) {
        //std::cout<<"Setting input info"<<std::endl;
        // Check quantities (truth, reco, missing, ...) --> not well defined yet
        double cs_dr       = 1;
        double cs_dphi     = TMath::Pi()/2.0;
        double cs_vpt      = 1;
        double cs_mbb      = 1;
        double cs_ptbb     = 1;
        float  cs_truthPt  = -1;  // dummy - to change
        double cs_ptb1     = 0.0;
        double cs_ptb2     = 0.0;
        double cs_met      = 1;
        double cs_njet     = 1;
        double cs_ntag     = 1;
        float  cs_avgTopPt = -1;  // dummy - to change

        cs_vpt = metVec.Pt();

        cs_met      = metVec.Pt();
        cs_njet     = jetsCentral.size();
        cs_ntag     = jetsCentralBTagged.size();

        truthVariablesCS(cs_truthPt, cs_avgTopPt);
        if(fatjets.size()>=1) {
            cs_mbb  = fatjets.at(0)->m();
            cs_ptbb = fatjets.at(0)->pt();
        }
        //event flavor not necessary to be set because it has already been set in the code that comes before this
        //setevent_flavour(selectedJets);
        //m_histNameSvc->set_eventFlavour(m_physicsMeta.b1Flav, m_physicsMeta.b2Flav);
        applyCS(cs_vpt, cs_mbb, cs_truthPt, cs_dphi, cs_dr, cs_ptb1, cs_ptb2, cs_met, cs_avgTopPt, cs_njet, cs_ntag);
    }






    //#############################################################
    //
    // Event Selection
    //
    //#############################################################

    /////////////////////////////////
    //Zero lepton selection
    /////////////////////////////////
    if(m_currentVar == "Nominal") {
        m_histSvc->BookFillHist("nelectrons_raw",  5,0,5, looseElectrons.size(), m_weight);
        for(const xAOD::Electron* ele: looseElectrons) {
            m_histSvc->BookFillHist("electron_pt_raw",  50,0,500, ele->pt()/1000., m_weight);
            m_histSvc->BookFillHist("electron_eta_raw",  100,-5,5, ele->eta(), m_weight);
            m_histSvc->BookFillHist("electron_phi_raw",  100,-1.*TMath::Pi(),1.*TMath::Pi(), ele->phi(), m_weight);
        }

        m_histSvc->BookFillHist("nmuons_raw",  5,0,5, looseMuons.size(), m_weight);
        for(const xAOD::Muon* mu: looseMuons) {
            m_histSvc->BookFillHist("muon_pt_raw",  50,0,500, mu->pt()/1000., m_weight);
            m_histSvc->BookFillHist("muon_eta_raw",  100,-5,5, mu->eta(), m_weight);
            m_histSvc->BookFillHist("muon_phi_raw",  100,-1.*TMath::Pi(),1.*TMath::Pi(), mu->phi(), m_weight);
        }
    }

    vector<TLorentzVector> leptons;
    leptons.clear();
    if(looseMuons.size()!=0 || looseElectrons.size()!=0) return EL::StatusCode::SUCCESS;


    double pileup(0.);
    if(!m_isMC) pileup = m_eventInfoReader->getObjects("Nominal")->auxdata<float>("corrected_averageInteractionsPerCrossing");
    else        pileup = m_eventInfoReader->getObjects("Nominal")->auxdata<float>("averageInteractionsPerCrossingRecalc");

    if(m_currentVar == "Nominal") {
        m_histSvc->BookFillHist("pileup_raw", 50,0,50, pileup, 1.0);
        m_histSvc->BookFillHist("pileupwgt_raw", 50,0,50, pileup, m_weight);

        m_histSvc->BookFillHist("nvtx_raw",  50,0,50, Props::NVtx2Trks.get(m_eventInfo), 	1.0) ;
        m_histSvc->BookFillHist("nvtxwgt_raw",  50,0,50, Props::NVtx2Trks.get(m_eventInfo), m_weight) ;
    }

    /////////////////////////////////
    // Cuts for event selection
    /////////////////////////////////

    // define general cuts
    bool mptCut = (mptVec.Pt()/1000.0>30.0);
    bool dPhiJetMetCut = min_dPhi>20.0*m_DegToRad;
    bool dPhiMetMptCut = fabs(metVec.DeltaPhi(mptVec))<90.0*m_DegToRad;
    bool rawMetCut = metVec.Pt()/1000.0 > 150 ; //trigger cuts


    if(m_currentVar == "Nominal") {
        m_histSvc->BookFillHist("mpt_presel",  100,0,500, mptVec.Pt()/1000.0, m_weight);
        m_histSvc->BookFillHist("dPhiJetMet_presel",  100,0,TMath::Pi(), min_dPhi, m_weight);
        m_histSvc->BookFillHist("dPhiMetMpt_presel",  100,0,TMath::Pi(), fabs(metVec.DeltaPhi(mptVec)), m_weight);

        m_histSvc->BookFillHist("met_presel",  200,150,2000, metVec.Pt()/1000.0, m_weight);
        m_histSvc->BookFillVarHist("metrebin_presel",   nBinsMET, &METBins[0], metVec.Pt()/1000.0, m_weight, true);
    }

    //####################################
    if( mptCut && dPhiJetMetCut && dPhiMetMptCut && rawMetCut ) {

        double LeadFatjetpt = 0;
        if(fatjetsTopandBTagged.size() > 0) LeadFatjetpt = fatjetsTopandBTagged.at(0)->pt();
        double balanceJMET = fabs( metVec.Pt() - LeadFatjetpt ) / metVec.Pt();


        // azimuthal distance between fatjet and MET
        double dPhiFatjetMet = -1;
        if(fatjetsTopandBTagged.size() > 0) {
            dPhiFatjetMet = fabs(fatjetsTopandBTagged.at(0)->p4().DeltaPhi(metVec));
            if(m_currentVar == "Nominal") m_histSvc->BookFillHist("dPhiFatjetMet_sel",  100,0,TMath::Pi(), dPhiFatjetMet, m_weight);
        }

        double mt_fatjetMET=0;
        if(fatjetsTopandBTagged.size() > 0) {
            float dphi = fabs(fatjetsTopandBTagged.at(0)->p4().DeltaPhi(metVec));
            mt_fatjetMET = TMath::Sqrt( 2 * (mptVec.Pt()/1000.0) * (fatjetsTopandBTagged.at(0)->pt()/1000) * (1-TMath::Cos(dphi)));
            if(m_currentVar == "Nominal") m_histSvc->BookFillHist("mtJMET_sel",  200,0.,2000, mt_fatjetMET , m_weight);

        }


        //############################
        //define boost cuts
        //############################
        bool metCut_boost = metVec.Pt()/1000.0 > 250.0 ;
        bool fatjetCut = (fatjetsTopandBTagged.size() == 1);
        bool dPhiFatjetMetCut = dPhiFatjetMet>2.8;
        bool balanceJMETCut = (balanceJMET<0.3);
        bool nExtraBjetsCut = (bjetsOutsideLeadfatjet.size()==0);

        if(m_currentVar == "Nominal") {
            m_histSvc->BookFillHist("met_sel",  200,150,2000, metVec.Pt()/1000.0, m_weight);
            m_histSvc->BookFillVarHist("metrebin_sel",   nBinsMET, &METBins[0], metVec.Pt()/1000.0, m_weight, true);
            m_histSvc->BookFillHist("metphi_sel",  200,-1.*TMath::Pi(),1.*TMath::Pi(), metVec.Phi(), m_weight);
            m_histSvc->BookFillHist("balanceJMET_sel",  200,0.,1., balanceJMET , m_weight);
            m_histSvc->BookFillHist("nfatjetsTopandBTagged_sel",  5,0,5, fatjetsTopandBTagged.size(), m_weight);
            m_histSvc->BookFillHist("nextrabjets_sel",  5,0,5, bjetsOutsideLeadfatjet.size(), m_weight);

            for(const xAOD::Jet* fjet: fatjetsTopandBTagged) {
                m_histSvc->BookFillHist("fatjet_pt_sel",   170,300,2000, fjet->pt()/1000., m_weight);
                m_histSvc->BookFillHist("fatjet_eta_sel",  50,-2.0,2.0, fjet->eta(), m_weight);
                m_histSvc->BookFillHist("fatjet_phi_sel",  50,-1.*TMath::Pi(),1.*TMath::Pi(), fjet->phi(), m_weight);
            }
        }

        if(metCut_boost && fatjetCut && dPhiFatjetMetCut /*&& balanceJMETCut*/ && nExtraBjetsCut) m_histSvc->BookFillHist("balanceJMET_nm1",  200,0.,1., balanceJMET , m_weight);

        if(metCut_boost && fatjetCut && dPhiFatjetMetCut && balanceJMETCut && nExtraBjetsCut) {
            float dPhiJMET = fabs(fatjetsTopandBTagged.at(0)->p4().DeltaPhi(metVec));
            float omega = ( metVec.Pt() - fatjetsTopandBTagged.at(0)->pt() ) / ( metVec.Pt() + fatjetsTopandBTagged.at(0)->pt() );

            m_histSvc->BookFillHist("metfinal",  200,200,2000, metVec.Pt()/1000.0, m_weight);
            m_histSvc->BookFillVarHist("metrebinfinal", nBinsMET, &METBins[0], metVec.Pt()/1000.0, m_weight, true);
            m_histSvc->BookFillVarHist("metrebinfinal2", nBinsMET2, &METBins2[0], metVec.Pt()/1000.0, m_weight, true);
            m_histSvc->BookFillHist("metphifinal",  200,-1.*TMath::Pi(),1.*TMath::Pi(), metVec.Phi(), m_weight);
            m_histSvc->BookFillHist("mJfinal",   100, 0.,500., fatjetsTopandBTagged.at(0)->m()/1000., m_weight);
            m_histSvc->BookFillHist("dphijmetfinal",  100,2,TMath::Pi(), dPhiJMET, m_weight);
            m_histSvc->BookFillHist("omegafinal",     100,-1.,1., omega, m_weight);
        }









        //############################
        //define resolved cuts
        //############################
        m_histNameSvc->set_description("Resolved");

        bool nofatjet = (fatjetsTopandBTagged.size() == 0);
        bool metCut_resolved   =  metVec.Pt()/1000.0 > 150.0 ;
        bool oneBjetCut = (jetsCentralBTagged.size()==1);

        int njetCentralNotBTagged = jetsCentralNotBTagged.size();
        int njetForward = jetsForward.size();
        bool centralJetCut = ( (njetCentralNotBTagged==2) && (njetForward==0) );

        double dPhiDijet = 0;
        double dPhiDijetMet = 0;
        double massDijet = 0;
        double ptDijet = 0;
        double massDijetBjet = 0;
        double leadingJetPt = 0;
        double trailingJetPt = 0;

        if(jetsCentralNotBTagged.size() >= 2) {
            dPhiDijet = fabs(jetsCentralNotBTagged.at(0)->p4().DeltaPhi(jetsCentralNotBTagged.at(1)->p4()));
            dPhiDijetMet = fabs((jetsCentralNotBTagged.at(0)->p4()+jetsCentralNotBTagged.at(1)->p4()).DeltaPhi(metVec));
            massDijet = (jetsCentralNotBTagged.at(0)->p4()+jetsCentralNotBTagged.at(1)->p4()).M();
            ptDijet = (jetsCentralNotBTagged.at(0)->p4()+jetsCentralNotBTagged.at(1)->p4()).Pt();
            if(jetsCentralBTagged.size()>0) massDijetBjet = (jetsCentralNotBTagged.at(0)->p4()+jetsCentralNotBTagged.at(1)->p4()+jetsCentralBTagged.at(0)->p4()).M();

            leadingJetPt = jetsCentralNotBTagged.at(0)->pt();
            trailingJetPt = jetsCentralNotBTagged.at(1)->pt();
        }
        bool dPhiDijetMetCut = dPhiDijetMet > 2.5;
        bool dPhiDijetCut = dPhiDijet > 0.2;



        if(nofatjet && oneBjetCut && centralJetCut) {
            for (const xAOD::Jet* bjet: jetsCentralBTagged) {
                for (const xAOD::Jet* jet: jetsCentralNotBTagged) {
                    double temp_dR = bjet->p4().DeltaR(jet->p4());
                    if(m_currentVar == "Nominal") m_histSvc->BookFillHist("jetbjetdr_presel",  100,0,2, temp_dR, m_weight);
                }
            }
        }

        if(nofatjet) {
            if(m_currentVar == "Nominal") {

                m_histSvc->BookFillHist("loosemet_presel",  200,150,2000, metVec.Pt()/1000.0, m_weight);
                m_histSvc->BookFillHist("nCentral_presel",  5,0,5, njetCentralNotBTagged, m_weight);
                m_histSvc->BookFillHist("nForward_presel",  5,0,5, njetForward, m_weight);
                m_histSvc->BookFillHist("dPhiDijet_presel", 200,0,TMath::Pi(), dPhiDijet, m_weight);
                m_histSvc->BookFillHist("dPhiDijetMet_presel",  200,0,TMath::Pi(), dPhiDijetMet, m_weight);
                m_histSvc->BookFillHist("massDijet_presel",  200,0,200, massDijet/1000.0, m_weight);
                m_histSvc->BookFillHist("ptDijet_presel",  200,0,800, ptDijet/1000.0, m_weight);
                m_histSvc->BookFillHist("ntrackjetsBTagged_presel",  5,0,5, jetsCentralBTagged.size(), m_weight);
                m_histSvc->BookFillHist("massDijetBjet_presel",  200,0,500, massDijetBjet/1000.0, m_weight);

                m_histSvc->BookFillHist("leadingJetPt_presel",  200,0,800, trailingJetPt/1000.0, m_weight);
                m_histSvc->BookFillHist("trailingJetPt_presel",  200,0,800, trailingJetPt/1000.0, m_weight);

            }
            if(metCut_resolved && centralJetCut && oneBjetCut && dPhiDijetMetCut) {

                m_histSvc->BookFillHist("loosemetfinal",  200,150,2000, metVec.Pt()/1000.0, m_weight);
                m_histSvc->BookFillHist("nCentralfinal",  5,0,5, njetCentralNotBTagged, m_weight);
                m_histSvc->BookFillHist("nForwardfinal",  5,0,5, njetForward, m_weight);
                m_histSvc->BookFillHist("dPhiDijetfinal", 200,0,TMath::Pi(), dPhiDijet, m_weight);
                m_histSvc->BookFillHist("dPhiDijetMetfinal",  200,0,TMath::Pi(), dPhiDijetMet, m_weight);
                m_histSvc->BookFillHist("massDijetfinal",  200,0,200, massDijet/1000.0, m_weight);
                m_histSvc->BookFillHist("ptDijetfinal",  200,0,800, ptDijet/1000.0, m_weight);
                m_histSvc->BookFillHist("ntrackjetsBTaggedfinal",  5,0,5, jetsCentralBTagged.size(), m_weight);

                m_histSvc->BookFillHist("leadingJetPtfinal",  200,0,800, trailingJetPt/1000.0, m_weight);
                m_histSvc->BookFillHist("trailingJetPtfinal",  200,0,800, trailingJetPt/1000.0, m_weight);

            }

        }



    }

    return EL::StatusCode::SUCCESS;
}//fill_monoTop_ZeroLepton



