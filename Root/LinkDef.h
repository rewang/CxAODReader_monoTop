#include <CxAODReader_monoTop/AnalysisReader_monoTop.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class AnalysisReader_monoTop+;
#endif
