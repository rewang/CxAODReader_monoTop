

#include "CxAODReader_monoTop/HistNameSvc_monoTop.h"

#include "TString.h"
// TODO random include to get Error() and Info(). Where is it coming from?
#include "AthContainers/AuxElement.h"

HistNameSvc_monoTop::HistNameSvc_monoTop() :
    HistNameSvc()
{
}


std::string HistNameSvc_monoTop::getFullHistName(const std::string& variable)
{

    m_name.clear();

    // SET IF NOMINAL OR SYS
    if (!m_isNominal) {

        if(!m_doOneSysDir) {
            m_name += "Sys";
            m_name += m_variation;
        } else m_name += "Systematics";

        m_name += "/";
    }

    TString tmp_sample = m_sample;
    tmp_sample.ReplaceAll("_","");
    m_name += tmp_sample;

    if (m_useEventFlav) {
        appendEventFlavour(m_name);
    }

    //number of smallR btags
    if (m_nTag < 0) m_name += "_0ptag";
    else if (m_nTag == 0) m_name += "_0tag";
    else if (m_nTag == 1) m_name += "_1tag";
    else if (m_nTag == 2) m_name += "_2tag";
    else m_name += "_3ptag";


    if (m_nFatJet < 0) {
        //Resolved region
        //inclusive in number of fatjets
        m_name += "0pfat";

        //real division based on number of small R jets
        if (m_nJet < 0) m_name += "0pjet";
        else if (m_nJet == 0) m_name += "0jet";
        else if (m_nJet == 1) m_name += "1jet";
//    else if (m_nJet == 2) m_name += "2jet";
//    else if (m_nJet == 3) m_name += "3jet";
        else m_name += "2pjet";
    } else {
        //Merged region
        //division always in terms of fat jets - we will only use 1pjets anyways
        if (m_nFatJet == 0) m_name += "0fat";
        else m_name += "1pfat";

        //also divide by the number of smallR jets
        //note that these are the jets not associated to the largeR jet
//   if (m_nJet < 0)
        m_name += "0pjet";
//    else if (m_nJet == 0) m_name += "0jet";
//    else if (m_nJet == 1) m_name += "1jet";
//    else if (m_nJet == 2) m_name += "2jet";
//    else if (m_nJet == 2) m_name += "3jet";
//   else if (m_nJet > 0 && m_nJet < 3) m_name += "2jet";
//   else  m_name += "3pjet";
    }

    //SAM:the pTV is actually the MET for us
    if (m_pTV < 0) m_name += "_0ptv_";
    else if (m_pTV < 150) m_name += "_0_150ptv_";
    else if (m_pTV < 250) m_name += "_150_200ptv_";
    else if (m_pTV < 350) m_name += "_200_350ptv_";
    else if (m_pTV < 500) m_name += "_350_500ptv_";
    else m_name += "_500ptv_";


    m_name += m_description;
    m_name += "_";
    m_name += variable;

    if (!m_isNominal) {
        m_name += "_Sys";
        m_name += m_variation;
    }

    return m_name;
}

void HistNameSvc_monoTop::appendEventFlavour(std::string& buff)
{
/*
//	std::cout << "hallo" << std::endl;
// if       (m_jet0flav < 0) return;
//   if       (m_jet0flav == 5 && m_jet1flav < 0) buff += "b";
//   else if  (m_jet0flav == 4 && m_jet1flav < 0) buff += "c";
    if  (m_jet0flav == 5 && m_jet1flav == 5)  buff += "bb";
    else if ((m_jet0flav == 5 || m_jet1flav == 5) && (m_jet0flav == 4 || m_jet1flav == 4)) buff += "bc";
    else if  (m_jet0flav == 5 || m_jet1flav == 5)  buff += "bl";
    else if  (m_jet0flav == 4 && m_jet1flav == 4)  buff += "cc";
    else if  (m_jet0flav == 4 || m_jet1flav == 4)  buff += "cl";
    else buff += "l";
*/

  if       (m_jet0flav < 0) return;
  if       (m_jet0flav == 5 && m_jet1flav < 0) buff += "b";
  else if  (m_jet0flav == 4 && m_jet1flav < 0) buff += "c";
  else if  (m_jet0flav == 5 && m_jet1flav == 5)  buff += "bb";
  else if ((m_jet0flav == 5 || m_jet1flav == 5)
        && (m_jet0flav == 4 || m_jet1flav == 4)) buff += "bc";
  else if  (m_jet0flav == 5 || m_jet1flav == 5)  buff += "bl";
  else if  (m_jet0flav == 4 && m_jet1flav == 4)  buff += "cc";
  else if  (m_jet0flav == 4 || m_jet1flav == 4)  buff += "cl";
  else buff += "l";

}

