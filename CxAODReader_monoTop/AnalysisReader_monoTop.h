#ifndef CxAODReader_AnalysisReader_monoTop_H
#define CxAODReader_AnalysisReader_monoTop_H

#include <CxAODTools/EventSelection.h>

#include <CxAODReader/AnalysisReader.h>
#include <CxAODReader_monoTop/HistNameSvc_monoTop.h>

#include "JetSubStructureUtils/BoostedXbbTag.h"
#include "JetSubStructureUtils/BosonTag.h"

#include <map>

#define CERRD std::cout<<"Error in "<<__FILE__<<" at "<<__LINE__<<std::endl;


class AnalysisReader_monoTop : public AnalysisReader {

protected:

    //SAM:Put here any functions that you are going to redefine in this derived class
    virtual EL::StatusCode initializeSelection() override;
    virtual EL::StatusCode initializeCorrsAndSysts () override;
    virtual EL::StatusCode initializeTools () override;
    virtual EL::StatusCode initializeIsMC () override;
    virtual EL::StatusCode initializeSumOfWeights() override;

    virtual EL::StatusCode setObjectsForOR(const xAOD::ElectronContainer* electrons,
                                           const xAOD::PhotonContainer* photons,
                                           const xAOD::MuonContainer* muons,
                                           const xAOD::TauJetContainer* taus,
                                           const xAOD::JetContainer* jets,
                                           const xAOD::JetContainer* fatjets);

    // this function pointer has to be set to some
    // fill function in initializeSelection()

    // code put here following https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/HiggsPhys/Run2/Hbb/CxAODFramework/CxAODReader/trunk/CxAODReader/AnalysisReader.h
    EL::StatusCode clearEmptyContainersWithNonZeroSize();

    //SAM:if you want to run more than one part of the analysis at a time, you have to
    //define more than one of the above objects. But that won't vibe with being a derived class
    //so leave it as one

    enum systematicVariation {RES, SCALE, NORM};
    enum systematicVariable {PT, MASS, D2};
    std::map<int,std::string> m_variationName;
    std::map<int,std::string> m_variableName;


    xAOD::Jet vary_FatJetSysts(const xAOD::Jet* fatjet, int var, int varType, double shift=0.);

    // fill functions
    EL::StatusCode fill_monoTop_ZeroLepton();
    EL::StatusCode fill_monoTop_OneLepton();
    EL::StatusCode fill_monoTop_TwoLepton();





    // corrections and systematics
    EL::StatusCode applyCS_v2 (float VpT,
                               float Mbb,
                               float ptbb,
                               float truthPt,
                               float DeltaPhi,
                               float pTB1,
                               int   njet,
                               int   ntag);
    EL::StatusCode applyCS (float VpT,
                            float Mbb,
                            float truthPt,
                            float DeltaPhi,
                            float DeltaR,
                            float pTB1,
                            float pTB2,
                            float MET,
                            float avgTopPt,
                            int   njet,
                            int   ntag);

    void truthVariablesCS(float &truthPt,float &avgTopPt);

    // helper functions
    void SetFatJetFlavor(std::vector<const xAOD::Jet*> fatjets);
    bool checkBlinding(const double& m);
    double checkWeight(const double& weight);
    double GetMETTriggerSF(const double& MET);

    // event shape variables
    double GetPThrust(std::vector<const xAOD::Jet*> jets, const xAOD::Jet* fatjet);

    //this member is set in the framework-read.cfg file
    //this determines which file is used to get the event yields
    //and is used to determine object selection
    std::string m_analysisType; //!

    // options (set in the framework-read.cfg file)
    bool m_blinding;          //!
    bool m_doSyst;            //!
    bool m_fullHists;         //!
    bool m_doCutflow;         //!
    bool m_doShapeVariables;  //!
    bool m_QCDHists;          //!

    // b-tagging
    double m_caloBTagLimit;    //!
    double m_trkBTagLimit;     //!

    // helper variables
    double m_RadToDeg;//!
    double m_DegToRad;//!

    void SetBTagTool(std::vector<const xAOD::Jet*> jets, std::string configList);
    void compute_btagging_calo(std::vector<const xAOD::Jet*> jets);
    void compute_btagging_track(std::vector<const xAOD::Jet*> jets);


    bool isBoostedJetTagged(double jpt, double jmass, double jtau32);
    float InterpolatorVal(float x, std::vector<float> m_xvalues, std::vector<float> m_yvalues);

    std::vector<std::string> m_csCorrections; //!
    std::vector<std::string> m_csVariations;  //!

public:
    std::string m_analysisRegion; //!

    // variables that don't get filled at submission time should be
    // protected from being send from the submission node to the worker
    // node (done by the //!)

public:
    // this is a standard constructor
    AnalysisReader_monoTop ();

    virtual EL::StatusCode histInitialize();

    virtual ~AnalysisReader_monoTop() {
    };

#ifndef __MAKECINT__
    HistNameSvc_monoTop* m_histNameSvc_monoTop; //!
#endif // not __MAKECINT_

    // this is needed to distribute the algorithm to the workers
    ClassDef(AnalysisReader_monoTop, 1);
};

#endif
