#ifndef HistNameSvc_monoTop_h
#define HistNameSvc_monoTop_h

#include <string>
#include "CxAODReader/HistNameSvc.h"

class HistNameSvc_monoTop : public HistNameSvc {

public:

    HistNameSvc_monoTop();
    ~HistNameSvc_monoTop() {};

    virtual std::string getFullHistName(const std::string& variable);
    virtual void appendEventFlavour(std::string& buff);
//   virtual void set_sample(const std::string& sample);

};

#endif
