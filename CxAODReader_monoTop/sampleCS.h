#ifndef CxAODReader_sampleCS
#define CxAODReader_sampleCS

class sampleCS {
public:

    sampleCS (std::string sampleName) {
        m_sampleName = sampleName;
    }

    void setup_sample (std::string sampleName, std::string detailsampleName) {
        if ((sampleName == "WlvH125")
                || (sampleName == "qqWlvH125")
                || (sampleName == "qqWminuseveHNLO125")
                || (sampleName == "qqWminusmuvmuHNLO125")
                || (sampleName == "qqWpluseveHNLO125")
                || (sampleName == "qqWplusmuvmuHNLO125")) m_sampleName = "WHlvbb";
        else if ((sampleName == "Zll125")
                 || (sampleName == "qqZllH125")) m_sampleName = "qqZHllbb";
        else if ((sampleName == "ZvvH125")
                 || (sampleName == "qqZvvH125")) m_sampleName = "qqZHvvbb";
        else if ((sampleName == "ggZeeH125")
                 || (sampleName == "ggZmmH125")
                 || (sampleName == "ggZttH125")) m_sampleName = "ggZHllbb";
        else if ((sampleName == "ggZveveH125")
                 || (sampleName == "ggZvmvmH125")
                 || (sampleName == "ggZvtvtH125")) m_sampleName = "ggZHvvbb";

        else if (sampleName == "W") m_sampleName = "W";
        else if (sampleName == "Z") m_sampleName = "Z";
        else if (sampleName == "Wv22") m_sampleName = "W";
        else if (sampleName == "Zv22") m_sampleName = "Z";
        else if (sampleName == "ttbar") m_sampleName = "ttbar";
        else if (sampleName == "stop_s") m_sampleName = "stop_s";
        else if (sampleName == "stop_t") m_sampleName = "stop_t";
        else if (sampleName == "stop_Wt") m_sampleName = "stop_Wt";

        else if (sampleName == "WW") m_sampleName = "WW";
        else if (sampleName == "WZ") m_sampleName = "WZ";
        else if (sampleName == "ZZ") m_sampleName = "ZZ";
        else if (sampleName == "WW_improved") m_sampleName = "WW";
        else if (sampleName == "WZ_improved") m_sampleName = "WZ";
        else if (sampleName == "ZZ_improved") m_sampleName = "ZZ";

        else if ((sampleName == "dijetJZW")
                 || (sampleName == "dijetZJ")) m_sampleName = "multijet";

        else m_sampleName = "NONAME";

//Sam added this line
        m_detailsampleName = m_sampleName;

//Sam commented these out because all samples had no detail samples name
//    if ((sampleName == "WW") || (sampleName == "WZ") || (sampleName == "ZZ")) m_detailsampleName = detailsampleName;
//    else m_detailsampleName = "NODETAILNAME";

    } // setup_sample

    void set_sampleName (std::string sampleName) {
        m_sampleName = sampleName;
    }

    void set_detailsampleName (std::string detailsampleName) {
        m_detailsampleName = detailsampleName;
    }

    std::string get_sampleName () {
        return m_sampleName;
    }

    std::string get_detailsampleName () {
        return m_detailsampleName;
    }

private:

    std::string m_sampleName;
    std::string m_detailsampleName;
};
#endif
